export const FETCH_TYPES = 'FETCH_TYPES'
export const TYPES_SUCCESS = 'TYPES_SUCCESS'
export const TYPES_ERROR = 'TYPES_ERROR'

import axios from 'axios'
import typeService from '../../shared/services/typeService'

const state = () => ({
  status: '',
  hasLoadedOnce: false,
  types: [],
})

const getters = {
  typesStatus: state => state.status,
  types: state => {
    return state.types
  }
}

const actions = {
  FETCH_TYPES: ({commit}) => {
    typeService.getTypes()
      .then(response => {
        commit(FETCH_TYPES, response.data.types)
      })
  },

}

const mutations = {
  FETCH_TYPES: (state, payload) => {
    state.types = payload
  },
  TYPES_SUCCESS: (state, resp) => {
    state.status = 'success'
    state.hasLoadedOnce = true
  },
  TYPES_ERROR: (state) => {
    state.status = 'error'
    state.hasLoadedOnce = true
  }
}

export default {
  state,
  getters,
  actions,
mutations}
