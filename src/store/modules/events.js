export const FETCH_EVENTS = 'FETCH_EVENTS'
export const FETCH_EVENT = 'FETCH_EVENT'
export const FETCH_ORGANIZER_PENDING_EVENTS = 'FETCH_ORGANIZER_PENDING_EVENTS'
export const FETCH_CLIENT_ACCEPTED_EVENTS = 'FETCH_CLIENT_ACCEPTED_EVENTS'
export const FETCH_COMPLETED_EVENTS = 'FETCH_COMPLETED_EVENTS'
export const CREATE_EVENT = 'CREATE_EVENT'
export const ACCEPT_EVENT = 'ACCEPT_EVENT'
export const DECLINE_EVENT = 'DECLINE_EVENT'
export const CONFIRM_EVENT = 'CONFIRM_EVENT'
export const CREATE_EVENT_COMMENT = 'CREATE_EVENT_COMMENT'
export const CREATE_EVENT_FEEDBACK = 'CREATE_EVENT_FEEDBACK'
export const EVENTS_SUCCESS = 'EVENTS_SUCCESS'
export const EVENTS_ERROR = 'EVENTS_ERROR'

import eventService from '../../shared/services/eventService'

const state = () => ({
  status: '',
  hasLoadedOnce: false,
  events: [],
  event: {},
  organizerEvents: [],
  clientAcceptedEvents: [],
  clientCompletedEvents: [],
})

const getters = {
  eventsStatus: state => state.status,
  events: state => {
    return state.events
  },
  event: state => {
    return state.event
  },
  organizerEvents: state => {
    return state.organizerEvents
  },
  clientAcceptedEvents: state => {
    return state.clientAcceptedEvents
  },
  clientCompletedEvents: state => {
    return state.clientCompletedEvents
  }
}

const actions = {
  FETCH_EVENTS: ({commit}) => {
    eventService.getEvents()
      .then(response => {
        commit(FETCH_EVENTS, response.data.events)
      })
  },
  FETCH_EVENT: ({commit}, id) => {
    eventService.getEvent(id)
    .then(response => {
      commit(FETCH_EVENT, response.data.event)
    })
  },
  FETCH_ORGANIZER_PENDING_EVENTS: ({commit}) => {
    eventService.getOrganizerEvents()
      .then(response => {
        commit(FETCH_ORGANIZER_PENDING_EVENTS, response.data.events)
      })
  },
  FETCH_CLIENT_ACCEPTED_EVENTS: ({commit}) => {
    eventService.getClientEvents('accepted')
      .then(response => {
        commit(FETCH_CLIENT_ACCEPTED_EVENTS, response.data.events)
      })
  },
  FETCH_COMPLETED_EVENTS: ({commit}) => {
    eventService.getClientEvents('completed')
      .then(response => {
        commit(FETCH_COMPLETED_EVENTS, response.data.events)
      })
  },
  CREATE_EVENT: ({commit}, event) => {
    eventService.createEvent(event)
    .then(response => {
      commit(CREATE_EVENT, response.data)
    })
  },
  ACCEPT_EVENT: ({commit}, event) => {
    eventService.acceptEvent(event)
    .then(response => {
      commit(ACCEPT_EVENT, response.data)
    })
  },
  DECLINE_EVENT: ({commit}, eventId) => {
    eventService.declineEvent(eventId)
    .then(response => {
      commit(DECLINE_EVENT, response.data)
    })
  },
  CONFIRM_EVENT: ({commit}, eventId) => {
    eventService.confirmEvent(eventId)
    .then(response => {
      commit(CONFIRM_EVENT, response.data)
    })
  },
  CREATE_EVENT_COMMENT: ({commit}, payload) => {
    console.log(payload)
    eventService.createComment(payload)
    .then(response => {
      commit(CREATE_EVENT_COMMENT, response.data)
    })
  },
  CREATE_EVENT_FEEDBACK: ({commit}, payload) => {
    console.log(payload)
    eventService.createFeedback(payload)
    .then(response => {
      commit(CREATE_EVENT_FEEDBACK, response.data)
    })
  },
}

const mutations = {
  FETCH_EVENTS: (state, payload) => {
    state.events = payload
  },
  FETCH_EVENT: (state, payload) => {
    state.event = payload
  },
  FETCH_ORGANIZER_PENDING_EVENTS: (state, payload) => {
    state.organizerEvents = payload
  },
  FETCH_CLIENT_ACCEPTED_EVENTS: (state, payload) => {
    state.clientAcceptedEvents = payload
  },
  FETCH_COMPLETED_EVENTS: (state, payload) => {
    state.clientCompletedEvents = payload
  },
  EVENTS_SUCCESS: (state, resp) => {
    state.status = 'success'
    state.hasLoadedOnce = true
  },
  EVENTS_ERROR: (state) => {
    state.status = 'error'
    state.hasLoadedOnce = true
  },
  CREATE_EVENT: () => {},
  DECLINE_EVENT: () => {},
  CONFIRM_EVENT: () => {},
  CREATE_EVENT_COMMENT: () => {},
  CREATE_EVENT_FEEDBACK: () => {},

}

export default {
  state,
  getters,
  actions,
mutations}
