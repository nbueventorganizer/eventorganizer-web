import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import users from './modules/users';
import events from './modules/events';
import types from './modules/types';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        events,
        types,
        users
    }
});