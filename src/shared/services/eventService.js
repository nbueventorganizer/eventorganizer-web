import { BASE_URL } from '../api'
import requester from '../requester'

const resourceUrl = `${BASE_URL}/events`

function getEvents (page = 1) {
  const serviceUrl = `${resourceUrl}/?page=${page}`
  return requester.get(serviceUrl)
}

function getEvent (id) {
  const serviceUrl = `${resourceUrl}/${id}`
  return requester.get(serviceUrl, true)
}

function getOrganizerEvents () {
  const serviceUrl = `${resourceUrl}/organizer`
  return requester.get(serviceUrl, true)
}

function getClientEvents (status) {
  const serviceUrl = `${resourceUrl}/client?status=${status}`
  return requester.get(serviceUrl, true)
}

function createEvent (event) {
  const serviceUrl = `${resourceUrl}`
  return requester.post(serviceUrl, event, true)
}

function declineEvent (eventId) {
  const serviceUrl = `${resourceUrl}/${eventId}/decline/`
  return requester.put(serviceUrl, {}, true)
}

function confirmEvent (eventId) {
  const serviceUrl = `${resourceUrl}/${eventId}/confirm/`
  return requester.put(serviceUrl, {}, true)
}

function acceptEvent (event) {
  const serviceUrl = `${resourceUrl}/${event.id}/accept/`
  return requester.put(serviceUrl, event, true)
}

function createComment (comment) {
  const serviceUrl = `${resourceUrl}/${comment.eventId}/comments`
  return requester.post(serviceUrl, comment, true)
}

function createFeedback (feedback) {
  const serviceUrl = `${resourceUrl}/${feedback.eventId}/feedbacks`
  return requester.post(serviceUrl, feedback, true)
}

export default {
  getEvents,
  getEvent,
  getOrganizerEvents,
  getClientEvents,
  createEvent,
  acceptEvent,
  declineEvent,
  confirmEvent,
  createComment,
  createFeedback
}
