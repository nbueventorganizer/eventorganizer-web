import { BASE_URL } from '../api'
import requester from '../requester'

const resourceUrl = `${BASE_URL}/types`

function getTypes (page = 1) {
  const serviceUrl = `${resourceUrl}`
  return requester.get(serviceUrl)
}
export default {
  getTypes,
}
