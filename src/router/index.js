import Vue from 'vue'
import Router from 'vue-router'

// Guards
import guest from '@/shared/guards/guest'
import auth from '@/shared/guards/auth'

// Auth
import register from '@/pages/auth/register'
import login from '@/pages/auth/login'

// Presentational
import home from '@/pages/home'
import createEvent from '@/pages/events/create'
import events from '@/pages/events/index'
import event from '@/pages/events/show'
import feedback from '@/pages/events/feedback'

// Organizer
import organizerPendingEvents from '@/pages/organizer/pending-events'
import organizerAcceptEvent from '@/pages/organizer/accept-event'
import organizerCompletedEvents from '@/pages/organizer/completed-events'
import uploadImages from '@/pages/organizer/gallery/upload'

// Client
import clientPendingEvents from '@/pages/client/pending-events'
import clientAcceptedEvents from '@/pages/client/accepted-events'
import clientCompletedEvents from '@/pages/client/completed-events'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    // Presentational
    { path: '/', name: 'home', component: home },
    { path: '/register', name: 'register', component: register, beforeEnter: guest},
    { path: '/login', name: 'login', component: login, beforeEnter: guest},
    { path: '/create-event', name: 'create-event', component: createEvent, beforeEnter: auth },
    { path: '/events', name: 'events', component: events },
    { path: '/events/:id', name: 'events.show', component: event },

    // Admin
    { path: '/admin/dashboard', name: 'admin.dashboard', component: clientPendingEvents },    
    { path: '/admin/users', name: 'admin.users.index', component: clientPendingEvents },    

    // Client
    { path: '/client/dashboard', name: 'client.dashboard', component: clientPendingEvents },    
    { path: '/client/account', name: 'client.account', component: clientPendingEvents },    
    { path: '/client/messages', name: 'client.messages', component: clientPendingEvents },    
    { path: '/client/pending-events', name: 'client.pending-events', component: clientPendingEvents },    
    { path: '/client/accepted-events', name: 'client.accepted-events', component: clientAcceptedEvents },    
    { path: '/client/completed-events', name: 'client.completed-events', component: clientCompletedEvents },    
    { path: '/client/completed-events/:id/feedback', name: 'client.event-feedback', component: feedback },    

    // Organizer
    { path: '/organizer/pending-events', name: 'organizer.pending-events', component: organizerPendingEvents },
    { path: '/organizer/pending-events/:id/accept', name: 'organizer.accept-event', component: organizerAcceptEvent },
    { path: '/organizer/completed-events/', name: 'organizer.completed-events', component: organizerCompletedEvents },
    { path: '/organizer/completed-events/:id/feedback', name: 'organizer.event-feedback', component: feedback },
    { path: '/organizer/upload', name: 'organizer.event.images.upload', component: uploadImages },

  ]
})
